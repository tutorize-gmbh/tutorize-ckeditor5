Tutorize CKEditor 5 classic editor build
========================================

## Developing ckeditor

1. Run `yarn install`
2. Make your changes, then run `yarn run build`
3. Adjust `package.json` in tbs to use your local installation of ckeditor: `"ckeditor5": "file:../tutorize-ckeditor5"`
4. Run `yarn upgrade ckeditor5` in tbs **every** time after creating a new ckeditor build

### Publish a new build

When you are done and want to publish a new version of ckeditor to use in tbs:

1. Run `yarn run build` to create a new ckeditor build
2. Increase the version in `package.json`. We use this scheme: `ckeditor_major.ckeditor_minor.ckeditor_hotfix-our_version`. So an adjustment on our version that is based on ckeditor `33.0.0` would be `33.0.0-0`, `33.0.0-1`, etc. Start at 0 again for every new ckeditor version.
3. Commit the new build
4. Add a new tag `git tag -a 'v33.0.0-0'`
5. Push `git push && git push --tags`
6. Set the new version in tbs `package.json`, run `yarn install` and commit `package.json` and `yarn.lock`

## Updating ckeditor

Periodically check [ckeditor5 changelog](https://github.com/ckeditor/ckeditor5/blob/master/CHANGELOG.md) for new
versions. When a new version is released:

1. Update all `ckeditor5` and all `@ckeditor*` dependencies in `package.json` to the latest version. Don't immediately
   update the other ones (webpacker etc) to their latest version!
2. Check [this package.json](https://github.com/ckeditor/ckeditor5/blob/master/packages/ckeditor5-build-classic/package.json)
and use the versions of the respective dev dependencies in our `package.json`
3. Run `yarn && yarn upgrade`
4. Check the changelog for any breaking changes
5. Update the version in our `package.json` to the respective ckeditor version
6. Publish a new build (see above)
