const colors = [
  {
    color: '#000000',
    label: 'Black'
  },
  {
    color: '#4d4d4d',
    label: 'Dim grey'
  },
  {
    color: '#999999',
    label: 'Grey'
  },
  {
    color: '#e6e6e6',
    label: 'Light grey'
  },
  {
    color: '#ffffff',
    label: 'White',
    hasBorder: true
  },
  {
    color: '#e64d4d',
    label: 'Red'
  },
  {
    color: '#e6994d',
    label: 'Orange'
  },
  {
    color: '#e6e64d',
    label: 'Yellow'
  },
  {
    color: '#99e64d',
    label: 'Light green'
  },
  {
    color: '#4de64d',
    label: 'Green'
  },
  {
    color: '#4de699',
    label: 'Aquamarine'
  },
  {
    color: '#4de6e6',
    label: 'Turquoise'
  },
  {
    color: '#4d99e6',
    label: 'Light blue'
  },
  {
    color: '#4d4de6',
    label: 'Blue'
  },
  {
    color: '#994de6',
    label: 'Purple'
  }
]

export const defaultConfig = {
  toolbar: {
    shouldNotGroupWhenFull: true,
    items: [
      'heading',
      'fontSize',
      'fontColor',
      'fontBackgroundColor',
      '|',
      'bold',
      'italic',
      'underline',
      'strikethrough',
      '|',
      'alignment',
      'outdent',
      'indent',
      'bulletedList',
      'numberedList',
      '|',
      'link',
      'insertTable',
      'tutorizeMediaUpload',
      'removeFormat',
      'undo',
      'redo'
    ]
  },
  fontSize: {
    options: [12, 'default', 16, 18, 20, 22, 24, 26, 28, 36, 48]
  },
  fontColor: {
    colors
  },
  fontBackgroundColor: {
    colors
  },
  image: {
    toolbar: [
      'toggleImageCaption',
      'imageTextAlternative',
      '|', 'imageStyle:block', 'imageStyle:alignLeft', 'imageStyle:alignRight'
    ],
    styles: ['alignLeft', 'alignRight']
  },
  table: {
    contentToolbar: [
      'tableColumn',
      'tableRow',
      'mergeTableCells',
      'tableProperties',
      'tableCellProperties'
    ]
  },
  // This value must be kept in sync with the language defined in webpack.config.js.
  language: 'en'
}
