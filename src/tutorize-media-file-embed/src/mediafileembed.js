import Plugin from '@ckeditor/ckeditor5-core/src/plugin'
import { toWidget } from '@ckeditor/ckeditor5-widget/src/utils'
import Widget from '@ckeditor/ckeditor5-widget/src/widget'

const mediaFileEmbedAttributes = ['data-media-file-json']

export default class TutorizeMediaFileEmbed extends Plugin {
  static get requires () {
    return [Widget]
  }

  init () {
    this._defineSchema()
    this._defineConverters()
  }

  _defineSchema () {
    const schema = this.editor.model.schema

    schema.register('mediaFileEmbed', {
      isObject: true,
      allowWhere: '$block',
      allowAttributes: mediaFileEmbedAttributes
    })
  }

  _defineConverters () {
    const conversion = this.editor.conversion
    const editingDowncastHandler = this.editor.config['_config']['mediaFileEmbed']['editingDowncast']

    conversion.for('upcast').elementToElement({
      view: {
        name: 'oembed',
        attributes: {
          'data-media-file-json': true
        }
      },
      model: (viewElement, { writer }) => {
        return writer.createElement('mediaFileEmbed', getElementMediaFileAttributes(viewElement))
      },
    })

    // runs when initializing the editor to create editable html for the editor
    conversion.for('editingDowncast').elementToStructure({
      model: 'mediaFileEmbed',
      view: (modelElement, { writer }) => {
        const element = editingDowncastHandler(modelElement, writer)
        return toWidget(element, writer, { label: 'simple box widget' })
      }
    })

    // runs when saving document to create viewable HTML for the user
    conversion.for('dataDowncast').elementToElement({
      model: 'mediaFileEmbed',
      view: (modelElement, { writer }) => {
        const previewElement = writer.createEmptyElement('oembed', {
          ...getElementMediaFileAttributes(modelElement)
        })

        return writer.createContainerElement('figure', { class: 'media' }, [previewElement])
      }
    })
  }
}

const getElementMediaFileAttributes = element => {
  const attributes = {}
  mediaFileEmbedAttributes.forEach(attribute => {
    attributes[attribute] = element.getAttribute(attribute)
  })
  return attributes
}
