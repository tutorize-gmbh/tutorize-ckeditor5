import '../theme/popup.css'

import { Plugin } from 'ckeditor5/src/core'

import TutorizeMediaUploadUI from './mediauploadui'
import TutorizeMediaFileEmbed from '../../tutorize-media-file-embed/src/mediafileembed'

export default class TutorizeMediaUpload extends Plugin {
  static get requires () {
    return [TutorizeMediaUploadUI, TutorizeMediaFileEmbed]
  }

  static get pluginName () {
    return 'TutorizeMediaUpload'
  }
}
