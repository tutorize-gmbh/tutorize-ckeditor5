import { Plugin } from 'ckeditor5/src/core'
import mediaIcon from '../theme/icons/media.svg'
import DropdownButtonView from '@ckeditor/ckeditor5-ui/src/dropdown/button/dropdownbuttonview'
import DropdownPanelView from '@ckeditor/ckeditor5-ui/src/dropdown/dropdownpanelview'
import DropdownView from '@ckeditor/ckeditor5-ui/src/dropdown/dropdownview'

export default class TutorizeMediaUploadUI extends Plugin {
  static get pluginName () {
    return 'TutorizeMediaUploadUI'
  }

  init () {
    const editor = this.editor

    editor.ui.componentFactory.add('tutorizeMediaUpload', locale => {
      const dropdown = createDropdown(locale)
      this._setUpDropdown(dropdown)

      return dropdown
    })
  }

  _setUpDropdown (dropdown) {
    const container = 'body'
    const directUploads = this.editor.config['_config']['mediaFileUpload']['directUploads']

    directUploads.classList.add('ckeditor--direct-uploads-popup')
    document.querySelector(container).appendChild(directUploads)

    function positionDirectUploads () {
      const pos = dropdown.element.getBoundingClientRect()
      const offset = getOffsetFromBoundingBox(pos)

      directUploads.style.top = `${offset.top}px`
      directUploads.style.left = `${offset.left}px`
    }

    dropdown.on('change:isOpen', () => {
      if (dropdown.isOpen) {
        positionDirectUploads()
        directUploads.style.display = 'block'
      } else {
        directUploads.style.display = 'none'
      }
    })
    directUploads.addEventListener('mediaPoolOpened', () => {
      directUploads.style.display = 'none'
    })
    window.addEventListener('resize', positionDirectUploads)

    function closeDropdown (e) {
      if (!dropdown.isOpen) return

      const path = e.composedPath()

      if (dropdown.element.contains(e.target) || directUploads.contains(e.target) || path.includes(directUploads)) {
        return
      }

      dropdown.isOpen = false
    }

    document.addEventListener('click', closeDropdown)

    dropdown.on('render', function () {
    })

    dropdown.buttonView.set({
      label: this.editor.t('Insert media'), icon: mediaIcon, tooltip: true
    })
  }
}

function getOffsetFromBoundingBox (box) {
  const docElem = document.documentElement

  return {
    top: box.top + box.height + (window.scrollY || docElem.scrollTop) - (docElem.clientTop || 0),
    left: box.left + (window.scrollX || docElem.scrollLeft) - (docElem.clientLeft || 0)
  }
}


function createDropdown (locale) {
  const buttonView = new DropdownButtonView(locale)
  const panelView = new DropdownPanelView(locale)
  const dropdownView = new DropdownView(locale, buttonView, panelView)

  buttonView.bind('isEnabled').to(dropdownView)

  if (buttonView instanceof DropdownButtonView) {
    buttonView.bind('isOn').to(dropdownView, 'isOpen')
  } else {
    buttonView.arrowView.bind('isOn').to(dropdownView, 'isOpen')
  }

  return dropdownView
}
