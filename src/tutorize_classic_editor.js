/**
 * This is a copy of ClassicEditorBase from '@ckeditor/ckeditor5-editor-classic/src/classiceditor'.
 * We cannot inherit from it because we need to adjust the constructor to use our own TutorizeClassicEditorUIView class.
 * Using this instead of ClassicEditorUIView is the only notable change compared to the original ClassicEditorBase.
 *
 * See TutorizeClassicEditorUIView for more details on why this is needed.
 **/

import { defaultConfig } from './default_config'
import { defaultPlugins } from './default_plugins'
import ClassicEditorUI from '@ckeditor/ckeditor5-editor-classic/src/classiceditorui'
import { CKEditorError, getDataFromElement, mix } from 'ckeditor5/src/utils'
import { attachToForm, DataApiMixin, Editor, ElementApiMixin } from 'ckeditor5/src/core'
import { isElement } from 'lodash-es'
import TutorizeClassicEditorUIView from './tutorize_classic_editor_ui_view'

export default class TutorizeClassicEditor extends Editor {
  constructor (sourceElementOrData, config = {}) {
    if (!isElement(sourceElementOrData) && config.initialData !== undefined) {
      throw new CKEditorError('editor-create-initial-data', null)
    }

    super(config)

    if (this.config.get('initialData') === undefined) {
      this.config.set('initialData', getInitialData(sourceElementOrData))
    }

    if (isElement(sourceElementOrData)) {
      this.sourceElement = sourceElementOrData
    }

    this.model.document.createRoot()

    const shouldToolbarGroupWhenFull = !this.config.get('toolbar.shouldNotGroupWhenFull')
    const view = new TutorizeClassicEditorUIView(this.locale, this.editing.view, {
      shouldToolbarGroupWhenFull
    })

    this.ui = new ClassicEditorUI(this, view)

    attachToForm(this)
  }

  destroy () {
    if (this.sourceElement) {
      this.updateSourceElement()
    }

    this.ui.destroy()

    return super.destroy()
  }

  static create (sourceElementOrData, config = {}) {
    return new Promise(resolve => {
      const editor = new this(sourceElementOrData, config)

      resolve(
        editor.initPlugins()
          .then(() => editor.ui.init(isElement(sourceElementOrData) ? sourceElementOrData : null))
          .then(() => editor.data.init(editor.config.get('initialData')))
          .then(() => editor.fire('ready'))
          .then(() => editor)
      )
    })
  }
}

mix(TutorizeClassicEditor, DataApiMixin)
mix(TutorizeClassicEditor, ElementApiMixin)

function getInitialData (sourceElementOrData) {
  return isElement(sourceElementOrData) ? getDataFromElement(sourceElementOrData) : sourceElementOrData
}

TutorizeClassicEditor.builtinPlugins = defaultPlugins
TutorizeClassicEditor.defaultConfig = defaultConfig
