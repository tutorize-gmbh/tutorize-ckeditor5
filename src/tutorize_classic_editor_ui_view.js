/**
 * This class is a mix of ClassicEditorUIView from '@ckeditor/ckeditor5-editor-classic/src/classiceditoruiview' and
 * BoxedEditorUIView from 'ckeditor/ckeditor5-ui/src/editorui/boxed/boxededitoruiview.js'.
 * We need it because we have to make adjustments to the ckeditor DOM. Normally we have something like this:
 *
 * div.ck.ck-editor__main
 *   > div.ck.ck-content
 *     > actual-editor-html-content
 *
 * But we need another layer to overlay the certificate background image, what we need is this:
 *
 * div.ck.ck-editor__main
 *   > div.ck.ck-editor__background_image_wrapper
 *     > div.ck.ck-content
 *       > actual-editor-html-content
 *
 * We use the div.ck.ck-editor__background_image_wrapper to set the actual background image. We then use
 * div.ck.ck-content to optionally overlay a white and partly transparent layer to mimic the background image transparency.
 *
 *
 * The only thing changed here is the insertion of the div.ck.ck-editor__background_image_wrapper.
 * Warning: this causes css issues because ckeditor has some css rules that go .ck-editor__main > .ck.ck-content.
 * We override & fix these in custom.css.
 *
 */
import { InlineEditableUIView, StickyPanelView, ToolbarView } from 'ckeditor5/src/ui'
import '@ckeditor/ckeditor5-editor-classic/theme/classiceditor.css'
import EditorUIView from '@ckeditor/ckeditor5-ui/src/editorui/editoruiview'
import LabelView from '@ckeditor/ckeditor5-ui/src/label/labelview'

export default class TutorizeClassicEditorUIView extends EditorUIView {
  constructor (locale, editingView, options = {}) {
    super(locale)

    this.top = this.createCollection()
    this.main = this.createCollection()
    this._voiceLabelView = this._createVoiceLabel()

    /**
     * Original DOM:
     *
     *    this.setTemplate( {
     * 			tag: 'div',
     *
     * 			attributes: {
     * 				class: [
     * 					'ck',
     * 					'ck-reset',
     * 					'ck-editor',
     * 					'ck-rounded-corners'
     * 				],
     * 				role: 'application',
     * 				dir: locale.uiLanguageDirection,
     * 				lang: locale.uiLanguage,
     * 				'aria-labelledby': this._voiceLabelView.id
     * 			},
     *
     * 			children: [
     * 				this._voiceLabelView,
     * 				{
     * 					tag: 'div',
     * 					attributes: {
     * 						class: [
     * 							'ck',
     * 							'ck-editor__top',
     * 							'ck-reset_all'
     * 						],
     * 						role: 'presentation'
     * 					},
     * 					children: this.top
     * 				},
     * 				{
     * 					tag: 'div',
     * 					attributes: {
     * 						class: [
     * 							'ck',
     * 							'ck-editor__main'
     * 						],
     * 						role: 'presentation'
     * 					},
     * 					children: this.main
     * 				}
     * 			]
     * 		} )
     */

    this.setTemplate({
      tag: 'div',

      attributes: {
        class: [
          'ck',
          'ck-reset',
          'ck-editor',
          'ck-rounded-corners'
        ],
        role: 'application',
        dir: locale.uiLanguageDirection,
        lang: locale.uiLanguage,
        'aria-labelledby': this._voiceLabelView.id
      },

      children: [
        this._voiceLabelView,
        {
          tag: 'div',
          attributes: {
            class: [
              'ck',
              'ck-editor__top',
              'ck-reset_all'
            ],
            role: 'presentation'
          },
          children: this.top
        },
        {
          tag: 'div',
          attributes: {
            class: [
              'ck',
              'ck-editor__main'
            ],
            role: 'presentation'
          },
          children: [
            {
              tag: 'div',
              attributes: {
                class: [
                  'ck',
                  'ck-editor__background_image_wrapper'
                ],
                role: 'presentation'
              },
              children: this.main
            }
          ]

        }
      ]
    })

    this.stickyPanel = new StickyPanelView(locale)
    this.toolbar = new ToolbarView(locale, {
      shouldGroupWhenFull: options.shouldToolbarGroupWhenFull
    })

    this.editable = new InlineEditableUIView(locale, editingView)
  }

  _createVoiceLabel () {
    const t = this.t
    const voiceLabel = new LabelView()

    voiceLabel.text = t('Rich Text Editor')

    voiceLabel.extendTemplate({
      attributes: {
        class: 'ck-voice-label'
      }
    })

    return voiceLabel
  }

  render () {
    super.render()

    // Set toolbar as a child of a stickyPanel and makes toolbar sticky.
    this.stickyPanel.content.add(this.toolbar)

    this.top.add(this.stickyPanel)
    this.main.add(this.editable)
  }
}
